cxxmodules-no-longer-experimental
---------------------------------

* C++ 20 named modules are now supported by :ref:`Ninja Generators`
  and :ref:`Visual Studio Generators` for VS 2022 and newer, in combination
  with the MSVC 14.34 toolset (provided with VS 17.4) and newer, LLVM/Clang
  16.0 and newer, and GCC 14 (after the 2023-09-20 daily bump) and newer.
  See :manual:`cmake-cxxmodules(7)` for details.
